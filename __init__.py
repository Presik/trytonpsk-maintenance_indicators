# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import indicators


def register():
    Pool.register(
        indicators.Equipment,
        indicators.IndicatorsByEquipmentStart,
        module='maintenance_indicators', type_='model')
    Pool.register(
        indicators.IndicatorsByEquipment,
        module='maintenance_indicators', type_='wizard')
